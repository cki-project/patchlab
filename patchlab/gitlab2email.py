# SPDX-License-Identifier: GPL-2.0-or-later
"""
This module deals with turning Gitlab objects (merge requests, comments) into
emails.
"""
from email import message_from_string, utils as email_utils
import logging
import re
import textwrap
import time
import urllib

from django.conf import settings
from django.core.mail import EmailMessage, get_connection
from django.core.mail.utils import DNS_NAME
from patchwork import parser as patchwork_parser
from patchwork.models import Submission
import gitlab as gitlab_module

from patchlab.models import GitForge, BridgedSubmission, Branch


_log = logging.getLogger(__name__)

PREFIX_RE = re.compile(r"^\[.*\]")

#: The template used when the number of commits in a merge request exceed
#: :data:`settings.PATCHLAB_MAX_EMAILS`. Instead of sending the patches, send
#: instructions on how to get the git branch.
BIG_EMAIL_TEMPLATE = """{description}
Note:

The patch series is too large to sent by email.

To review the series locally, set up your repository to fetch from the GitLab
remote:

  $ git remote add gitlab {remote_url}
  $ git config remote.gitlab.fetch '+refs/merge-requests/*/head:refs/remotes/gitlab/merge-requests/*'
  $ git fetch gitlab

Finally, check out the merge request:

  $ git checkout gitlab/merge-requests/{merge_id}

It is also possible to review the merge request on GitLab at:
    {merge_url}
"""  # noqa: E501


def email_merge_request(
    gitlab: gitlab_module.Gitlab, forge_id: int, merge_id: int
) -> None:
    """Email a merge request to a mailing list."""
    try:
        git_forge = GitForge.objects.get(
            host=urllib.parse.urlsplit(gitlab.url).hostname, forge_id=forge_id
        )
    except GitForge.DoesNotExist:
        _log.error(
            "Request to bridge merge id %d from project id %d on %s cannot "
            "be handled as no git forge is configured in Patchlab's database.",
            merge_id,
            forge_id,
            urllib.parse.urlsplit(gitlab.url).hostname,
        )
        return
    project = gitlab.projects.get(forge_id)
    merge_request = project.mergerequests.get(merge_id)

    if _ignore(git_forge, merge_request):
        return

    # This is all pretty hacky, but works for now. Just hang out until the
    # pipeline is completed.
    initial_head = merge_request.sha
    if settings.PATCHLAB_PIPELINE_SUCCESS_REQUIRED:
        for _ in range(settings.PATCHLAB_PIPELINE_MAX_WAIT):
            if merge_request.head_pipeline["status"] not in ("failed", "success"):
                _log.info(
                    "Pipeline for %r is %s, checking back in a minute",
                    merge_request,
                    merge_request.head_pipeline["status"],
                )
                time.sleep(60)
            else:
                break
            merge_request = project.mergerequests.get(merge_id)
        else:
            _log.warn(
                "Pipeline failed to complete after %d minutes; not emailing %r",
                settings.PATCHLAB_PIPELINE_WAIT,
                merge_request,
            )
            return

    merge_request = project.mergerequests.get(merge_id)
    if initial_head != merge_request.sha:
        _log.info(
            "A new revision for %r has been pushed, skipping emailing revision %s",
            merge_request,
            initial_head,
        )
        return
    if _ignore(git_forge, merge_request):
        # A label might have been added or something while we waited for CI.
        return

    emails = _prepare_emails(gitlab, git_forge, project, merge_request)
    with get_connection(fail_silently=False) as conn:
        for email in emails:
            try:
                submission = _record_bridging(git_forge.project.listid, merge_id, email)
            except ValueError:
                # This message is already in the database, skip sending it
                continue
            email.connection = conn
            try:
                email.send(fail_silently=False)
                _log.info("Sent email '%s' for merge request %r", email.subject, merge_request)
            except Exception as e:
                # We were unable to send the email, delete it from the submission db
                # and raise the exception so we try again
                submission.delete()
                _log.info("Failed to send email '%s' for merge request %r", email.subject, merge_request)
                raise e


def _ignore(git_forge, merge_request):
    if merge_request.work_in_progress:
        _log.info("Not emailing %r because it's a work in progress", merge_request)
        return True
    if merge_request.merge_status == "cannot_be_merged":
        _log.info("Not emailing %r because it can't be merged", merge_request)
        return True
    if (
        settings.PATCHLAB_PIPELINE_SUCCESS_REQUIRED
        and merge_request.head_pipeline["status"] == "failed"
    ):
        _log.info("Not emailing %r as the test pipeline failed", merge_request)
        return True
    if "From email" in merge_request.labels:
        _log.info("Not emailing %r as it's from email to start with", merge_request)
        return True
    for label in settings.PATCHLAB_IGNORE_GITLAB_LABELS:
        if label in merge_request.labels:
            _log.info(
                "Not emailing %r as it is labeled with the %s label, which is ignored.",
                merge_request,
                label,
            )
            return True
    if BridgedSubmission.objects.filter(
        git_forge=git_forge, merge_request=merge_request.iid, commit=merge_request.sha
    ).first():
        _log.info(
            "Not emailing %r as the head sha is %s, which we already bridged.",
            merge_request,
            merge_request.sha,
        )
        return True

    return False


def _reroll(git_forge, merge_request):
    """Determine the patch reroll count based on previous submissions."""
    prior_submissions = BridgedSubmission.objects.filter(
        git_forge=git_forge, merge_request=merge_request.iid
    )
    latest_submission = prior_submissions.order_by("-series_version").first()
    if latest_submission is None:
        version = 1
    else:
        if latest_submission.series_version:
            version = latest_submission.series_version + 1
        else:
            version = 1

    return version


def _merge_request_ccs(git_forge, merge_request):
    """Collect merge request Ccs"""
    ccs = []
    if merge_request.description is not None:
        for line in merge_request.description.splitlines():
            cc_match = re.search(r"^Cc:\s+(.*)$", line)
            if cc_match:
                ccs += cc_match.groups()

    ccs += [line[3:].strip() for line in merge_request.labels if line.startswith("Cc:")]
    return _clean_ccs(ccs)


def _commit_ccs(commit):
    ccs = [commit.author_email]
    for line in commit.message.splitlines():
        cc_match = re.search(r"^(Cc|Signed-off-by|Reviewed-by):\s+(.*)$", line)
        if cc_match and cc_match.group(2).strip():
            ccs.append(cc_match.group(2).strip())

    return _clean_ccs(ccs)


def _clean_ccs(ccs):
    ccs = [email_utils.parseaddr(cc)[1] for cc in ccs if email_utils.parseaddr(cc)[1]]
    ccs = list(
        set(
            [
                cc
                for cc in ccs
                if re.search(settings.PATCHLAB_CC_FILTER, cc, flags=re.IGNORECASE)
            ]
        )
    )
    ccs.sort()
    return ccs


def _get_payload(diffs):
    """ Turn a list of diffs into patch text. """
    diff = ""

    for path in diffs:
        diff += f"diff --git a/{path['old_path']} b/{path['new_path']}\n"
        if path['new_file']:
            diff += f"new file mode {path['b_mode']}\n"
        if path['renamed_file']:
            diff += f"rename from {path['old_path']}\n"
            diff += f"rename to {path['new_path']}\n"
        if path['deleted_file']:
            diff += f"deleted file mode {path['a_mode']}\n"
        diff += f"index blahblah..blahblah {path['b_mode']}\n"
        if path['new_file']:
            diff += "--- /dev/null\n"
        else:
            diff += f"--- a/{path['old_path']}\n"
        if path['deleted_file']:
            diff += "+++ /dev/null\n"
        else:
            diff += f"+++ b/{path['new_path']}\n"
        diff += path['diff']

    return diff


# Derived from https://www.mercurial-scm.org/repo/hg/file/tip/mercurial/patch.py
def diffstatsum(stats):
    maxfile, maxtotal, addtotal, removetotal, binary = 0, 0, 0, 0, False
    for f, a, r, b in stats:
        maxfile = max(maxfile, len(f))
        maxtotal = max(maxtotal, a + r)
        addtotal += a
        removetotal += r
        binary = binary or b

    return maxfile, maxtotal, addtotal, removetotal, binary


def diffstatdata(lines):
    diffre = re.compile(r'^diff .*-r [a-z0-9]+\s(.*)$')
    gitre = re.compile(r'diff --git a/(.*) b/(.*)')

    results = []
    filename, adds, removes, isbinary = None, 0, 0, False

    def addresult():
        if filename:
            results.append((filename, adds, removes, isbinary))

    # inheader is used to track if a line is in the
    # header portion of the diff.  This helps properly account
    # for lines that start with '--' or '++'
    inheader = False

    for line in lines.split('\n'):
        if line.startswith('diff'):
            addresult()
            # starting a new file diff
            # set numbers to 0 and reset inheader
            inheader = True
            adds, removes, isbinary = 0, 0, False
            if line.startswith('diff --git a/'):
                filename = gitre.search(line).group(2)
            elif line.startswith('diff -r'):
                # format: "diff -r ... -r ... filename"
                filename = diffre.search(line).group(1)
        elif line.startswith('@@'):
            inheader = False
        elif line.startswith('+') and not inheader:
            adds += 1
        elif line.startswith('-') and not inheader:
            removes += 1
        elif line.startswith('GIT binary patch') or line.startswith(
            'Binary file'
        ):
            isbinary = True
        elif line.startswith('rename from'):
            filename = line[12:]
        elif line.startswith('rename to'):
            filename += ' => %s' % line[10:]
    addresult()
    return results


def diffstat(lines, width=80):
    output = []
    stats = diffstatdata(lines)
    maxname, maxtotal, totaladds, totalremoves, hasbinary = diffstatsum(stats)

    countwidth = len(str(maxtotal))
    if hasbinary and countwidth < 3:
        countwidth = 3
    graphwidth = width - countwidth - maxname - 6
    if graphwidth < 10:
        graphwidth = 10

    def scale(i):
        if maxtotal <= graphwidth:
            return i
        # If diffstat runs out of room it doesn't print anything,
        # which isn't very useful, so always print at least one + or -
        # if there were at least some changes.
        return max(i * graphwidth // maxtotal, int(bool(i)))

    for filename, adds, removes, isbinary in stats:
        if isbinary:
            count = 'Bin'
        else:
            count = '%d' % (adds + removes)
        pluses = '+' * scale(adds)
        minuses = '-' * scale(removes)
        output.append(
            ' %s%s |  %*s %s%s\n'
            % (
                filename,
                ' ' * (maxname - len(filename)),
                countwidth,
                count,
                pluses,
                minuses,
            )
        )

    if stats:
        output.append(
            (' %d files changed, %d insertions(+), %d deletions(-)\n')
            % (len(stats), totaladds, totalremoves)
        )

    return ''.join(output)


DEP_PFX = "Dependencies::"
def _get_dep_scope(merge_request):
    """Get Dependencies label scope for MR."""
    for label in merge_request.labels:
        if label.startswith(DEP_PFX):
            return label.split("::")[-1]
    return None


def _get_dep_label_created_at(merge_request, scope):
    """Get the created_at timestamp for the Deps label."""
    evt_at = None
    label_events = merge_request.resourcelabelevents.list(all=True)
    for evt in label_events:
        if evt.label and evt.label['name'] == f'{DEP_PFX}{scope}':
            evt_at = evt.created_at

    return evt_at


def _prepare_emails(gitlab, git_forge, project, merge_request):
    """Prepare a set of emails that represent the given merge request."""
    try:
        branch = Branch.objects.get(
            git_forge=git_forge, name=merge_request.target_branch
        )
    except Branch.DoesNotExist:
        # Branch isn't configured to be bridged, skip.
        _log.info(
            "There is no branch in the patchlab database for %s; skipping",
            merge_request.target_branch,
        )
        return []

    from_email = settings.PATCHLAB_FROM_EMAIL.format(
        forge_user=merge_request.author["username"]
    )
    from_header = f"\"{merge_request.author['name']} (via Email Bridge)\" <{from_email}>"

    author_public_email = gitlab.users.get(merge_request.author['id']).public_email
    if author_public_email and author_public_email.endswith('@redhat.com'):
        reply_to_header = f"\"{merge_request.author['name']}\" <{author_public_email}>"
    else:
        reply_to_header = None

    mr_has_deps = False
    hit_dep_sha = False
    deps = _get_dep_scope(merge_request)
    # if no deps, throw exception, message should be requeued
    if deps is None:
        raise Exception(f'Dependencies label for MR {merge_request.iid} not present')

    dep_label_created_at = _get_dep_label_created_at(merge_request, deps)
    mr_updated_at = merge_request.updated_at
    if deps != "OK":
        mr_has_deps = True
    commits = list(reversed(list(merge_request.commits())))
    truncated_note = ""
    new_commits = []
    for commit in commits:
        if mr_has_deps:
            ref = project.commits.get(commit.id)
            if len(ref.parent_ids) > 1:
                continue
            if not hit_dep_sha:
                if commit.id.startswith(deps):
                    hit_dep_sha = True
                continue
        if commit.committer_email != author_public_email:
            # stop after the first commit that does not match the MR author
            # this restriction limits email bombs by MRs pointing to the
            # wrong base or target branch (turning a single email into 10K)
            if not author_public_email:
                truncated_note = \
                    ("NOTE: Truncated patchset due to missing public @redhat.com email\n"
                     "      address on your GitLab profile at https://gitlab.com/-/profile.\n"
                     "      Once that is fixed, close and reopen the merge request to\n"
                     "      retrigger sending the emails.\n")
            elif len(commits) > 2000:
                truncated_note = \
                    ("NOTE: Truncated patchset since you are most likely targeting the\n"
                     "      wrong branch in GitLab. Open the merge request URL in your web\n"
                     "      browser, click the Edit button at the top of the page and change\n"
                     "      the target branch at the top of that page.\n")
            else:
                truncated_note = \
                    (f"NOTE: Truncated patchset since committer email '{commit.committer_email}'\n"
                      "      does not match the submitter's GitLab public email address\n"
                     f"      '{author_public_email}'.")

            break
        new_commits.append(commit)

    if mr_has_deps and not hit_dep_sha and dep_label_created_at < mr_updated_at:
        raise Exception(f'Dependencies label for MR {merge_request.iid} not up to date')

    # Save the number of old_commits to determine if filtering occured
    # and add a trailer to the cover letter to let the author know.
    num_old_commits = len(commits)

    commits = new_commits
    num_commits = len(commits)
    series_version = _reroll(git_forge, merge_request)
    version_prefix = f"v{series_version}" if series_version > 1 else ""

    ccs = _merge_request_ccs(git_forge, merge_request)

    # Increment the timeval by one for each message generated so that the messages appear
    # in the correct order on the client.
    timeval = time.time()

    emails = []
    if num_commits > 1 or num_old_commits > 1:
        # Compose a cover letter based on the pull request description.
        headers = {
            "Date": email_utils.formatdate(timeval=timeval, localtime=settings.EMAIL_USE_LOCALTIME),
            "Message-ID": email_utils.make_msgid(domain=DNS_NAME),
            "X-Patchlab-Merge-Request": f"{merge_request.web_url}",
            "X-Patchlab-Series-Version": series_version,
        }
        if reply_to_header:
            headers["Reply-To"] = reply_to_header
        timeval += 1

        DIFFSTAT_RE = re.compile(r" \d* files? changed, (\d* insertions?\(\+\))?(, )?(\d* deletions?\(\-\))?")
        desc_has_diffstat = False
        # We have to wrap each line rather than just using plain textwrap.fill
        # on the whole description as doing so destroys paragraphs and wraps
        # any Ccs into a sentence, which isn't what we want.
        if merge_request.description is not None:
            wrapped_description = "\n".join(
                [
                    textwrap.fill(line, width=78, replace_whitespace=False)
                    for line in merge_request.description.splitlines()
                ]
            )
            for line in merge_request.description.splitlines():
                match = DIFFSTAT_RE.match(line)
                if match:
                    desc_has_diffstat = True
                    break
        else:
            wrapped_description = "The merge request had no description."

        mr_diffs = merge_request.diffs.list()
        mr_diff_ids = []
        for mr_diff in mr_diffs:
            mr_diff_ids.append(mr_diff.id)
        mr_diff_ids.sort()
        for mr_diff_id in mr_diff_ids:
            mr_diff_data = merge_request.diffs.get(mr_diff_id)

        mr_diff_strings = _get_payload(mr_diff_data.diffs)
        mr_diffstat = ""
        diffstat_with_deps_note = \
            ("\nWARNING: this diffstat includes MR dependencies."
             f"\nAsk {merge_request.author['name']} to include a proper diffstat if need be.")
        if not desc_has_diffstat:
            mr_diffstat = "\n---\n" + diffstat(mr_diff_strings)
            if mr_has_deps:
                mr_diffstat += diffstat_with_deps_note
        body = (
            f"From: {merge_request.author['name']} on {git_forge.host}\n"
            f"Merge Request: {merge_request.web_url}\n"
            f"{truncated_note}"
            "\n"
            f"{wrapped_description}\n"
            f"{mr_diffstat}\n"
        )
        subject = (
            f"[{branch.subject_prefix} PATCH{version_prefix} 0/{num_commits}] "
            f"{' '.join(merge_request.title.splitlines())}"  # No multi-line headers allowed
        )
        cover_letter = EmailMessage(
            subject=subject,
            body=body,
            from_email=from_header,
            to=[git_forge.project.listemail],
            cc=ccs,
            headers=headers,
            reply_to=[git_forge.project.listemail],
        )

        if (series_version == 1 and num_commits > 2000) or \
           (series_version > 1 and num_commits > settings.PATCHLAB_MAX_EMAILS):
            cover_letter.body = BIG_EMAIL_TEMPLATE.format(
                description=body or "No description provided for merge request.",
                remote_url=f"{project.web_url}.git",
                merge_id=merge_request.iid,
                merge_url=merge_request.web_url,
            )
            return [cover_letter]

        in_reply_to = headers["Message-ID"]
        emails.append(cover_letter)
    else:
        in_reply_to = None

    for i, commit in enumerate(commits, 1):
        c = project.commits.get(commit.id)
        diffs = c.diff()

        patch_num = "" if len(commits) == 1 else f" {str(i)}/{num_commits}"
        sanitized_patch_title = " ".join(commit.title.splitlines())
        subject = (
            f"[{branch.subject_prefix} PATCH{version_prefix}{patch_num}] "
            f"{sanitized_patch_title}"
        )
        patch_author = f"{commit.author_name} <{commit.author_email}>"
        patch_ccs = sorted(list(set(_commit_ccs(commit) + ccs)))
        # Ensure anyone getting Cc'd on a patch also gets the cover letter
        if num_commits > 1:
            emails[0].cc = sorted(list(set(emails[0].cc + patch_ccs)))

        headers = {
            "Date": email_utils.formatdate(timeval=timeval, localtime=settings.EMAIL_USE_LOCALTIME),
            "Message-ID": email_utils.make_msgid(domain=DNS_NAME),
            "X-Patchlab-Patch-Author": patch_author,
            "X-Patchlab-Merge-Request": f"{merge_request.web_url}",
            "X-Patchlab-Commit": f"{commit.id}",
            "X-Patchlab-Series-Version": series_version,
        }
        if reply_to_header:
            headers["Reply-To"] = reply_to_header
        if in_reply_to:
            headers["In-Reply-To"] = in_reply_to
        timeval += 1

        payload = _get_payload(diffs)
        body = f"From: {patch_author}\n\n{commit.message}\n{payload}\n--\n{merge_request.web_url}"
        email = EmailMessage(
            subject=subject,
            body=body,
            from_email=from_header,
            to=[git_forge.project.listemail],
            cc=patch_ccs,
            headers=headers,
            reply_to=[git_forge.project.listemail],
        )
        emails.append(email)

    return emails


def email_comment(gitlab, forge_id, author, comment, merge_id=None) -> None:
    """Email a comment made on Gitlab"""
    try:
        git_forge = GitForge.objects.get(
            host=urllib.parse.urlsplit(gitlab.url).hostname, forge_id=forge_id
        )
    except GitForge.DoesNotExist:
        _log.error(
            "Got comment event for project id %d, which isn't in the database", forge_id
        )
        return

    commit = comment.get("commit_id")
    try:
        bridged_submission = BridgedSubmission.objects.filter(
            git_forge=git_forge
        ).order_by("-series_version")
        if merge_id:
            bridged_submission = bridged_submission.filter(merge_request=merge_id)
        if commit:
            bridged_submission = bridged_submission.filter(commit=commit)
        bridged_submission = bridged_submission[0]
    except IndexError:
        _log.info(
            "Unable to find a bridged submission for comment on MR %d, commit %s, forge %r",
            merge_id,
            commit,
            git_forge,
        )
        return

    project = gitlab.projects.get(forge_id)
    if merge_id:
        merge_request = project.mergerequests.get(merge_id)
        ccs = _merge_request_ccs(git_forge, merge_request)
    else:
        ccs = []

    from_email = settings.PATCHLAB_FROM_EMAIL.format(forge_user=author["name"])
    from_header = f"\"{author['name']} (via Email Bridge)\" <{from_email}>"
    # From the bridged_submission, find the in-reply-to, create email.
    headers = {
        "Date": email_utils.formatdate(localtime=settings.EMAIL_USE_LOCALTIME),
        "Message-ID": email_utils.make_msgid(domain=DNS_NAME),
        "In-Reply-To": bridged_submission.submission.msgid,
        "X-Patchlab-Comment": comment["url"],
    }
    subject = "Re: " + " ".join(
        message_from_string(bridged_submission.submission.headers)[
            "Subject"
        ].splitlines()
    )
    wrapped_description = "\n".join(
        [
            textwrap.fill(line, width=78, replace_whitespace=False)
            for line in comment["note"].splitlines()
        ]
    )
    body = (
        f"From: {author['name']} on {git_forge.host}\n{comment['url']}\n\n{wrapped_description}\n"
        f""
    )
    comment = EmailMessage(
        subject=subject,
        body=body,
        from_email=from_header,
        to=[git_forge.project.listemail],
        cc=ccs,
        headers=headers,
        reply_to=[git_forge.project.listemail],
    )
    with get_connection(fail_silently=False) as conn:
        patchwork_parser.parse_mail(comment.message(), list_id=git_forge.project.listid)
        comment.connection = conn
        comment.send(fail_silently=False)


def _record_bridging(listid: str, merge_id: int, email: EmailMessage) -> None:
    """
    Create the Patchwork submission records. This would happen when the mail
    hit the mailing list, but doing so now lets us associate them with a
    BridgedSubmission so we can post follow-up comments.

    Raises:
        ValueError: If the emails cannot be parsed by patchwork or is a duplicate.
        Submission.DoesNotExist: If the Submission object isn't created by
            patchwork; this indicates Patchwork has changed in some way or
            there's a bug in this function.
    """
    try:
        patchwork_parser.parse_mail(email.message(), list_id=listid)
    except patchwork_parser.DuplicateMailError:
        _log.error(
            "Message ID %s is already in the database; do not call "
            "_record_bridging twice with the same email",
            email.extra_headers["Message-ID"],
        )
        raise ValueError(email)

    try:
        submission = Submission.objects.get(msgid=email.extra_headers["Message-ID"])
    except Submission.DoesNotExist:
        _log.error(
            "Patchwork did not save the email with Message-ID '%s' which "
            "likely means the subject match field on the project with listid "
            "'%s' is filtering out emails with subjects like '%s'",
            email.extra_headers["Message-ID"],
            listid,
            email.subject,
        )
        raise

    bridged_submission = BridgedSubmission(
        submission=submission,
        git_forge=submission.project.git_forge,
        merge_request=merge_id,
        commit=email.extra_headers.get("X-Patchlab-Commit"),
        series_version=email.extra_headers.get("X-Patchlab-Series-Version", 1),
    )
    bridged_submission.save()
    return bridged_submission
