# SPDX-License-Identifier: GPL-2.0-or-later
import logging
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from patchlab.views.processor import process_merge_request
from patchlab.views.processor import process_pipeline
from patchlab.views.processor import process_comment
from cki_lib.messagequeue import MessageQueue

LOGGER = logging.getLogger(__name__)

# pylint: disable=unused-argument
def process_message(routing_key, payload, webhooks):
    """Process a webhook message."""
    object_kind = payload.get('object_kind')
    if not object_kind:
        return False  # unit tests
    if object_kind not in webhooks:
        return False  # unit tests

    webhooks[object_kind](payload)

    return True  # unit tests

WEBHOOKS = {
        "merge_request": process_merge_request,
        "note": process_pipeline,
        "note": process_comment,
}


class Command(BaseCommand):
    help = (
        "Covert a patch series to GitLab merge requests; this should happen"
        " automatically, but if recovering from a crash in the automated handler"
        " during mail import, this can be useful."
    )

    def add_arguments(self, parser):
        parser.add_argument("--rabbitmq-host", 
                            default=settings.RABBITMQ_HOST,
                            help="Rabbitmq host")
        parser.add_argument("--rabbitmq-port", 
                            default=settings.RABBITMQ_PORT,
                            help="Rabbitmq port")
        parser.add_argument("--rabbitmq-user", 
                            default=settings.RABBITMQ_USER,
                            help="Rabbitmq user")
        parser.add_argument("--rabbitmq-password", 
                            default=settings.RABBITMQ_PASSWORD,
                            help="Rabbitmq password")
        parser.add_argument("--rabbitmq-exchange", 
                            default=settings.RABBITMQ_EXCHANGE,
                            help="Rabbitmq exchange")
        parser.add_argument("--rabbitmq-queue-name", 
                            default=settings.RABBITMQ_QUEUE_NAME,
                            help="Rabbitmq queue name")
        parser.add_argument("--rabbitmq-routing-key", 
                            default=settings.RABBITMQ_ROUTING_KEYS,
                            help="Rabbitmq routing keys")

    def handle(self, *args, **kwargs):

        def _queue_processor(routing_key, payload):
            LOGGER.info('Processing message from queue: %s', payload)
            process_message(routing_key, payload, WEBHOOKS)

        host = kwargs['rabbitmq_host']
        port = kwargs['rabbitmq_port']
        user = kwargs['rabbitmq_user']
        password = kwargs['rabbitmq_password']
        exchange = kwargs['rabbitmq_exchange']
        queue_name = kwargs['rabbitmq_queue_name']
        routing_keys = kwargs['rabbitmq_routing_key']

        if not (host or port or user or password or exchange or queue_name or
           routing_keys):
               LOGGER.error('Missing rabbitmq parameters')

        try:
            queue = MessageQueue(host, port, user, password)
            LOGGER.info("Waiting for messages")
            queue.consume_messages(exchange, routing_keys.split(),
                                   _queue_processor, queue_name)
        except Exception as e:
            raise CommandError(f"Failed to open merge request: {str(e)}")
